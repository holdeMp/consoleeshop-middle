using NUnit.Framework;
using System.IO;
using System;
using System.Linq;
namespace ConsoleEShop_Middle
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Login_ValidData_ChangeUserStatus()
        {
            CurrentUser.UserStatus = CurrentUser.Status.Guest;
            var input = new StringReader(@"zxcqwe
123456");
            Console.SetIn(input);

            Actions.DoAction("4");
            Assert.AreEqual(CurrentUser.UserStatus.ToString(), User.Status.Logged.ToString());
        }
        [Test]
        public void Login_InvalidData_MessageAboutIncorrectLoginData()
        {
            CurrentUser.UserStatus = CurrentUser.Status.Guest;
            var output = new StringWriter();
            Console.SetOut(output);          
            var input = new StringReader(@"zxcqwe
123556
3");
            Console.SetIn(input);

            Actions.DoAction("4");
            var output1 = output.ToString().Split("\n");
            Assert.AreEqual("Incorrect username or password\r", output1[3]);
        }
        [Test]
        public void Register_EnterNewUserData_RegisteredUsersContainsNewUsername()
        {
            CurrentUser.UserStatus = CurrentUser.Status.Guest;
            var input = new StringReader(@"newuser
123556
123556
dsapop@pos");
            Console.SetIn(input);

            Actions.DoAction("3");
            Assert.That(Collections.RegisteredUsers.ContainsKey("newuser"));
        }
        [Test]
        public void Register_UsernameAlreadyRegistered_MessageAboutIncorrectUsername()
        {
            CurrentUser.UserStatus = CurrentUser.Status.Guest;
            var output = new StringWriter();
            Console.SetOut(output);
            var input = new StringReader(@"zxcqwe
2");
            Console.SetIn(input);

            Actions.DoAction("3");
            var temp = output.ToString().Split(",");
            var temp2 = temp[0].Split(":");
            Assert.AreEqual("\r\nUsername already registreted ", temp2[1]); 
        }
        [Test]
        public void WatchProducts_CollectionsProducts_FirstProductNameSamsung()
        {
            var output = new StringWriter();
            Console.SetOut(output);
            var input = new StringReader(@"
");
            Console.SetIn(input);
            Actions.DoAction("1");

            var output1 = output.ToString().Split("\n");
            var res = output1[1];
            Assert.AreEqual("Name: Samsung Galaxy A51 6/128GB Blue\r", res);
        }
        [Test]
        public void SearchProducts_CollectionsProducts_SuccesfullySearchBoschProduct()
        {
            var output = new StringWriter();
            Console.SetOut(output);
            var input = new StringReader(@"bosch
3");
            Console.SetIn(input);


            Actions.DoAction("2");
            var output1 = output.ToString().Split("\n");
            var res = output1[4];
            Assert.AreEqual("Name: Built-in dishwasher BOSCH SMV25EX00E\r", res);
        }
        [Test]
        public void MakeNewOrder_CollectionsOrder_AddedNewOrder()
        {
            

            var input = new StringReader(@"bosch");
            Console.SetIn(input);
            CurrentUser.UserStatus = CurrentUser.Status.Logged;
            Actions.DoAction("3");
            Assert.That(Collections.orders.Any(i => i.Good.Name == "Built-in dishwasher BOSCH SMV25EX00E"));
        }
        [Test]
        public void ConfirmOrder_ConfirmedOrders_AddedNewConfirmedOrder()
        {
            CurrentUser.UserStatus = CurrentUser.Status.Logged;
            var input = new StringReader(@"bosch
1");
            Console.SetIn(input);
            Actions.DoAction("3");
            Actions.DoAction("4");
            Assert.That(Collections.ConfirmedOrders.Any(i => i.Value.Any(i => i.Good.Name == "Built-in dishwasher BOSCH SMV25EX00E")));
        }
        [Test]
        public void AddNewProduct_CollectionsGoods_AddedNewGoodWithNameTPlink()
        {
            CurrentUser.UserStatus = CurrentUser.Status.Administator;

            var input = new StringReader(@"TP-LINK TG-3468
Appliances
The TG-3468 Gigabit Network Adapter is the most cost effective integrated PCIe Ethernet adapter, fully compatible with IEEE 802.3, IEEE 802.3u, and IEEE 802.3ab devices.
288");
            Console.SetIn(input);

            Actions.DoAction("6");
            Assert.That(Collections.goods.Any(i => i.Name == "TP-LINK TG-3468"));
        }
        [Test]
        public void ChangeUserPersonalInfo_PersonalInfos_ChangedNameAndSurnameForAdmin()
        {
            CurrentUser.UserStatus = CurrentUser.Status.Administator;
            var input = new StringReader(@"admin
1
Vill
2
Benj

");
            Console.SetIn(input);

            Actions.DoAction("5");
            Assert.That(Collections.PersonalInfos["admin"].Name == "Vill" &&
                Collections.PersonalInfos["admin"].Surname == "Benj");
        }
        [Test]
        public void ChangeStatusOfOrder_ConfirmedOrders_ChangeOrderStatusToCanceledByAdmin()
        {
            CurrentUser.UserStatus = CurrentUser.Status.Logged;
            var input = new StringReader(@"bosch
1
1
1
");
            Console.SetIn(input);

            Actions.DoAction("3");
            Actions.DoAction("4");
            CurrentUser.UserStatus = CurrentUser.Status.Administator;
            Actions.DoAction("8");
            Assert.That(Collections.ConfirmedOrders[1].Any(i => i.OrderStatus == OrderStatus.CanceledByAdmin));
        }
    }
}