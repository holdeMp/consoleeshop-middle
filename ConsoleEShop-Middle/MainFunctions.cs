﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle
{
    public static class MainFunctions
    {
        public static void WatchProducts()
        {
            
            Displayer.DisplayGoods();
            Console.WriteLine("Press empty string for back \n");
            while (true)
            {
                switch (Console.ReadLine())
                {
                    case "":
                        return;
                    default:
                        Console.WriteLine("Incorrect button");
                        break;
                }
            }
        }

        public static void SearchProduct()
        {

            string res = Displayer.DisplaySearchingGood();
            if (res == "3")
            {
                
                return;
            }
            if (res == "Success")
            {
                Console.WriteLine("\nSuccesful searching . Press 1 for repeat , 3 for back");
                while (true)
                {
                    switch (Console.ReadLine())
                    {
                        case "1":
                            SearchProduct();
                            return;

                        case "3":
                            
                            return;
                        default:
                            Console.WriteLine("Incorrect button");
                            break;
                    }
                }
            }
        }
        public static void GoBack()
        {
            try
            {
                Console.Clear();
            }
            catch { Console.WriteLine("Incorrect descriptor"); }
            Displayer displayer = new Displayer();
            displayer.DisplayOptions();
            CycleOptions.Cycle();

        }

    }
}
