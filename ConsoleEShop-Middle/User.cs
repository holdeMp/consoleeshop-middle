﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle
{
    public class User
    {
        public enum Status
        {
            Logged,
            Administator,
            Guest
        }
        public static Status UserStatus { get; set; } = Status.Guest;
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }

        public User(string Username, string Password, string Email) {
            this.Username = Username;
            this.Password = Password;
            this.Email = Email;
        }
    }
}
