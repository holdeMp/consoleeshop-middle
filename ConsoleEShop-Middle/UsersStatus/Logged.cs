﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Text.RegularExpressions;

namespace ConsoleEShop_Middle
{
    public class Logged : IMainFunctionality, IRegisteredUserFunctions
    {
        public void SearchProduct()
        {
            MainFunctions.SearchProduct();
        }
        public void MakeNewOrder()
        {
            RegisteredUserFunctions.MakeNewOrder();
        }
        public void WatchProducts()
        {
            MainFunctions.WatchProducts();
        }
        public void ConfirmOrCancelOrder()
        {
            RegisteredUserFunctions.ConfirmOrCancelOrder();
        }

        public void QuitFromAccount()
        {
            RegisteredUserFunctions.QuitFromAccount();
        }

        public static bool IsPhoneNumber(string number)
        {
            return number.All(char.IsDigit);
        }
        public static void ChangePersonalInfo()
        {

            Displayer.DisplayPersonalInfo(CurrentUser.Username);
            Console.WriteLine("Press number of field to change or empty string to back:");
            while (true)
            {
                switch (Console.ReadLine())
                {
                    case "":
                        MainFunctions.GoBack();
                        return;
                    case "1":
                        Console.WriteLine("Enter new name");
                        Collections.PersonalInfos[CurrentUser.Username].Name = Console.ReadLine();
                        Console.WriteLine("Succesful changing name");
                        break;
                    case "2":
                        Console.WriteLine("Enter new surname");
                        Collections.PersonalInfos[CurrentUser.Username].Surname = Console.ReadLine();
                        Console.WriteLine("Succesful changing surname");
                        break;
                    case "3":
                        ChangePhoneNumber();
                        break;
                    case "4":
                        Console.WriteLine("Enter new adress");
                        Collections.PersonalInfos[CurrentUser.Username].Adress = Console.ReadLine();
                        Console.WriteLine("Succesful changing adress");
                        break;
                    case "5":
                        Console.WriteLine("Enter new email");
                        ChangeEmail();
                        break;
                    default:
                        Console.WriteLine("Incorrect button");
                        break;


                }
            }
        }
        public static void ChangeEmail()
        {
            string email = Console.ReadLine();
            if (!IsValidEmail(email))
            {
                Console.WriteLine("Incorrect email ,Repeat :\n 1-Yes\n 2-No");
                while (true)
                {
                    switch (Console.ReadLine())
                    {
                        case "1":
                            ChangeEmail();
                            return;
                        case "2":
                            MainFunctions.GoBack();
                            return;
                        default:
                            break;
                    }
                }
            }
            Collections.PersonalInfos[CurrentUser.Username].Email=email;
            Console.WriteLine("Succesfully changed email");
        }
        private static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
        private static void ChangePhoneNumber()
        {
            Console.WriteLine("Enter new phone number");
            string number = Console.ReadLine();
            if (IsPhoneNumber(number))
            {
                Collections.PersonalInfos[CurrentUser.Username].PhoneNumber = number;
                Console.WriteLine("Succesful changing phone number");
                return;
            }
            else
            {
                Console.WriteLine("Incorrect number , try again 1 , 2 change another field,3 go back ");
                while (true)
                {
                    switch (Console.ReadLine())
                    {
                        case "1":
                            ChangePhoneNumber();
                            return;
                        case "2":
                            Console.Clear();
                            ChangePersonalInfo();
                            return;
                        case "3":
                            MainFunctions.GoBack();
                            return;
                        default:
                            Console.WriteLine("Incorrect button");
                            break;
                    }
                }
            }
        }
        public static void SetReceivedStatus()
        {
            if (!Collections.ConfirmedOrders.Any())
            {
                Console.WriteLine("You don't have confirmed orders\nPress empty string for back");
                while (true)
                {
                    switch (Console.ReadLine())
                    {
                        case "":
                            MainFunctions.GoBack();
                            return;
                        default:
                            Console.WriteLine("Incorrect button");
                            break;
                    }
                }
            }
            Displayer.DisplayConfirmedOrders(CurrentUser.Username);
            Console.WriteLine("Press empty string for back");
            Console.WriteLine("Press id order to received");
            string id = Console.ReadLine();
            if (id == "")
            {
                
                return;
            }
            if (!Collections.ConfirmedOrders.ContainsKey(Convert.ToInt32(id)))
            {
                Console.WriteLine("There is no order with such id");
                Console.WriteLine("Press 1 for repeat , 2 for back");
                while (true)
                {
                    switch (Console.ReadLine())
                    {
                        case "1":
                            SetReceivedStatus();
                            return;
                        case "2":
                            
                            return;
                        default:
                            Console.WriteLine("Incorrect button");
                            break;
                                

                    }
                }
            }
            SetStatusConfirmedOrder(Convert.ToInt32(id), OrderStatus.Received);
            Console.WriteLine("Succesful setting received status...");
            Thread.Sleep(1000);
            
        }
        public static void WatchHistoryAndStatus()
        {
            if (!Collections.ConfirmedOrders.Any())
            {
                Console.WriteLine("You don't have confirmed orders\nPress empty string for back");
                while (true)
                {
                    switch (Console.ReadLine())
                    {
                        case "":
                            MainFunctions.GoBack();
                            return;
                        default:
                            Console.WriteLine("Incorrect button");
                            break;
                    }
                }
            }
            Displayer.DisplayConfirmedOrders(CurrentUser.Username);
            Console.WriteLine("\nPress empty string for back");
            while (true)
            {
                switch (Console.ReadLine())
                {
                    case "":
                        MainFunctions.GoBack();
                        return;
                    default:
                        Console.WriteLine("Incorrect button");
                        break;
                }
            }
        }
        public bool CheckGoodForExisting(string query)
        {
            foreach (var good in Collections.goods)
            {
                if (good.Name.ToLower().Contains(query.ToLower()))
                {
                    
                    return true;
                }

            }
            return false;
        }
        public static void SetStatusConfirmedOrder(int id, OrderStatus orderStatus)
        {
            foreach(var ConfirmedOrder in Collections.ConfirmedOrders[id])
            {
                ConfirmedOrder.OrderStatus = orderStatus;
            }
        }
    }
}
