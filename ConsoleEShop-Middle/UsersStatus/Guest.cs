﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace ConsoleEShop_Middle
{
    public class Guest:IMainFunctionality
    {
        public static void Registration()
        {
            Console.WriteLine("Press 3 for back");
            string username = FillUserName();
            if (username == null || username == "3")
            {
                Console.Clear();
                Displayer displayer = new Displayer();
                displayer.DisplayOptions();
                CycleOptions.Cycle();
                return;
            }
            string password = FillPass();
            if (password == null || password == "3")
            {
                Console.Clear();
                Displayer displayer = new Displayer();
                displayer.DisplayOptions();
                CycleOptions.Cycle();
                return ;
            }
            string email = FillEmail();
            if (email == null || email == "3")
            {
                Console.Clear();
                Displayer displayer = new Displayer();
                displayer.DisplayOptions();
                CycleOptions.Cycle();
                return;
            }
            Collections.RegisteredUsers.Add(username, new User(username, password, email));
            Collections.PersonalInfos.Add(username, new PersonalInfo { Email=email});
            Console.WriteLine("Succesful registration...");
            Thread.Sleep(1000);                     
        }
        public static void Login()
        {
            Console.WriteLine("Press 3 for back\nEnter username: ");
            string username = Console.ReadLine();
            if (username == "3")
            {
                Console.Clear();
                Displayer displayer = new Displayer();
                displayer.DisplayOptions();
                return;
            }
            Console.WriteLine("Enter password:");
            string password = Console.ReadLine();
            if (!Collections.RegisteredUsers.ContainsKey(username) || Collections.RegisteredUsers[username].Password!=password)
            {
                Console.WriteLine("Incorrect username or password");
                Console.WriteLine( "Press 1 for repeat or 3 for back");
                while (true)
                {
                    switch (Console.ReadLine())
                    {
                        case "1":
                            Login();
                            return ;
                        case "3":
                            
                            return;
                        default:
                            Console.WriteLine("Incorrect button");
                            break;
                    }
                }
            }
            else
            {
                Console.WriteLine("Succesful login...");
                Thread.Sleep(1000);
                if (username == "admin")
                {
                    CurrentUser.UserStatus = CurrentUser.Status.Administator;
                    CurrentUser.Username = username;
                    CurrentUser.Email = Collections.RegisteredUsers[username].Email;
                }
                else
                {
                    CurrentUser.UserStatus = CurrentUser.Status.Logged;
                    CurrentUser.Username = username;
                    CurrentUser.Email = Collections.RegisteredUsers[username].Email;
                }
                
            }
        }
        private static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
        private static string FillUserName()
        {
            Console.WriteLine("Enter username:");
            string username = Console.ReadLine();
            if (username == "3")
            {
                return username;
            }
            if (username.Length < 3)
            {
                Console.WriteLine("Username length must be more than 2 symbols ,Repeat :\n 1-Yes\n 2-No");
                switch (Console.ReadLine())
                {
                    case "1":
                        FillEmail();
                        break;
                    case "2":
                        return null;
                }
            }
            if (Collections.RegisteredUsers.ContainsKey(username))
            {
                Console.WriteLine("Username already registreted ,Repeat :\n 1-Yes\n 2-No");
                switch (Console.ReadLine())
                {
                    case "1":
                        FillEmail();
                        break;
                    case "2":
                        return null;
                }
            }
            return username;
        }
        private static string FillEmail()
        {
            Console.WriteLine("Enter email:");

            string email = Console.ReadLine();
            if (email == "3")
            {

                return email;
            }
            if (!IsValidEmail(email))
            {
                Console.WriteLine("Incorrect email ,Repeat :\n 1-Yes\n 2-No");
                switch (Console.ReadLine())
                {
                    case "1":
                        FillEmail();
                        break;
                    case "2":
                        return null;
                }
            }
            return email;

        }
        private static string FillPass()
        {
            Console.WriteLine("Enter password:");
            string password1 = Console.ReadLine();
            if (password1 == "3")
            {
                return password1;
            }
            if (password1.Length < 6)
            {
                Console.WriteLine("Passwords length must be more than 5 ,Repeat :\n 1-Yes\n 2-No");
                switch (Console.ReadLine())
                {
                    case "1":
                        string pass = FillPass();
                        return pass;
                    case "2":
                        return null;
                }
            }
            Console.WriteLine("Enter password again:");
            string password2 = Console.ReadLine();
            if (password2 == "3")
            {
                return password1;
            }
            if (password1 != password2)
            {
                Console.WriteLine("Passwords don't match ,Repeat :\n 1-Yes\n 2-No");
                while (true)
                {
                    switch (Console.ReadLine())
                    {
                        case "1":
                            FillPass();
                            break;
                        case "2":
                            return null;
                        default:
                            Console.WriteLine("Incorrect button");
                            break;
                    }
                }
            }
            return password1;
        }

        public void WatchProducts()
        {
            MainFunctions.WatchProducts();
        }

        public void SearchProduct()
        {
            MainFunctions.SearchProduct();
        }
    }
}
