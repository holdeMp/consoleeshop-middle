﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;

namespace ConsoleEShop_Middle
{
    public class Admin : IMainFunctionality,IRegisteredUserFunctions
    {
        public void ConfirmOrCancelOrder()
        {
            RegisteredUserFunctions.ConfirmOrCancelOrder();
        }

        public void MakeNewOrder()
        {
            RegisteredUserFunctions.MakeNewOrder();
        }

        public void QuitFromAccount()
        {
            RegisteredUserFunctions.QuitFromAccount();
        }

        public void SearchProduct()
        {
            MainFunctions.SearchProduct();
        }
        public static void ChangeUsersPersonalInfo()
        {
            Displayer.DisplayUsersPersonalInfo();
            Console.WriteLine("Enter a username of User to change or empty string to back");
            string username = Console.ReadLine();
            if (username == "")
            {
                
                return;

            }
            if (!Collections.PersonalInfos.ContainsKey(username))
            {
                Console.WriteLine("There is no user with such username");
                Console.WriteLine("Press 1 for repeat , empty string for back");
                while (true)
                {
                    switch (Console.ReadLine())
                    {
                        case "1":
                            ChangeUsersPersonalInfo();
                            return;
                        case "":
                            
                            return;
                        default:
                            Console.WriteLine("Incorrect button");
                            break;
                    }
                }
            }
            ChangePersonalInfo(username);
        }
        public static void ChangePersonalInfo(string username)
        {

            Displayer.DisplayPersonalInfo(username);
            Console.WriteLine("Press number of field to change or empty string to back:");
            while (true)
            {
                switch (Console.ReadLine())
                {
                    case "":
                        
                        return;
                    case "1":
                        Console.WriteLine("Enter new name");
                        Collections.PersonalInfos[username].Name = Console.ReadLine();
                        Console.WriteLine("Succesful changing name");
                        break;
                    case "2":
                        Console.WriteLine("Enter new surname");
                        Collections.PersonalInfos[username].Surname = Console.ReadLine();
                        Console.WriteLine("Succesful changing surname");
                        break;
                    case "3":
                        ChangePhoneNumber(username);
                        break;
                    case "4":
                        Console.WriteLine("Enter new adress");
                        Collections.PersonalInfos[username].Adress = Console.ReadLine();
                        Console.WriteLine("Succesful changing adress");
                        break;
                    case "5":
                        Console.WriteLine("Enter new email");
                        ChangeEmail(username);
                        break;
                    default:
                        Console.WriteLine("Incorrect button");
                        break;


                }
            }
        }
        public static void ChangeEmail(string username)
        {
            string email = Console.ReadLine();
            if (!IsValidEmail(email))
            {
                Console.WriteLine("Incorrect email ,Repeat :\n 1-Yes\n 2-No");
                while (true)
                {
                    switch (Console.ReadLine())
                    {
                        case "1":
                            ChangeEmail(username);
                            return;
                        case "2":
                            MainFunctions.GoBack();
                            return;
                        default:
                            break;
                    }
                }
            }
            Collections.PersonalInfos[username].Email = email;
            Console.WriteLine("Succesfully changed email");
        }
        private static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
        public static bool IsPhoneNumber(string number)
        {
            return number.All(char.IsDigit);
        }

        private static void ChangePhoneNumber(string username)
        {
            Console.WriteLine("Enter new phone number");
            string number = Console.ReadLine();
            if (IsPhoneNumber(number))
            {
                Collections.PersonalInfos[username].PhoneNumber = number;
                Console.WriteLine("Succesful changing phone number");
                return;
            }
            else
            {
                Console.WriteLine("Incorrect number , try again 1 , 2 change another field,3 go back ");
                while (true)
                {
                    switch (Console.ReadLine())
                    {
                        case "1":
                            ChangePhoneNumber(username);
                            return;
                        case "2":
                            Console.Clear();
                            ChangePersonalInfo(username);
                            return;
                        case "3":
                            MainFunctions.GoBack();
                            return;
                        default:
                            Console.WriteLine("Incorrect button");
                            break;
                    }
                }
            }
        }

        public static void ChangeStatusOfOrder()
        {
            if (!Collections.ConfirmedOrders.Any())
            {
                Console.WriteLine("There is no orders");
                Thread.Sleep(1000);
                return;
            }
            foreach(var order in Collections.ConfirmedOrders)
            {
                var user = order.Value.Select(i => i.User).Select(i => i).First().Username;
                var status = order.Value.Select(i => i.OrderStatus).Select(i => i).First();
                Console.WriteLine("Order Id: "+order.Key);
                Console.WriteLine("User of order: " + user);
                Console.WriteLine("Order Status: " + status);
            }
            Console.WriteLine("Enter id order to change or empty string for back");
            string id = Console.ReadLine();
            if (id == "")
            {
                Console.WriteLine("Incorrect id");
                Thread.Sleep(1000);
                
                return;
            }
            try { Convert.ToInt32(id); }
            catch 
            {
                Console.WriteLine("Incorrect id");
                Thread.Sleep(1000);
                
                return;
            }
            if (!Collections.ConfirmedOrders.Any(i => i.Key== Convert.ToInt32(id))) 
            {
                Console.WriteLine("There is no order with such id\nPress 1 for repeat,empty string for back");
                while (true)
                {
                    switch (Console.ReadLine())
                    {
                        case "":
                            MainFunctions.GoBack();
                            return;
                        case "1":
                            ChangeStatusOfOrder();
                            return;
                        default:
                            Console.WriteLine("Incorrect button");
                            break;

                    }
                }
            }
            Console.WriteLine("Enter a number of status to set or empty string for back");
        //    Received,
        //New,
        //CanceledByAdmin,
        //Paid,
        //Sent,
        //Completed,
        //CanceledByUser
            Console.WriteLine("1-CanceledByAdmin\n2-Paid\n3-Sent\n4-Completed");
            while (true)
            {
                switch (Console.ReadLine())
                {
                    case "1":
                        SetStatusConfirmedOrder(Convert.ToInt32(id), OrderStatus.CanceledByAdmin);
                        Console.WriteLine("Succesfully change order status");
                        Thread.Sleep(1300);
                        return;
                    case "2":
                        SetStatusConfirmedOrder(Convert.ToInt32(id), OrderStatus.Paid);
                        Console.WriteLine("Succesfully change order status");
                        Thread.Sleep(1300);
                        
                        return;
                    case "3":
                        SetStatusConfirmedOrder(Convert.ToInt32(id), OrderStatus.Sent);
                        Console.WriteLine("Succesfully change order status");
                        Thread.Sleep(1300);
                        return;
                    case "4":
                        SetStatusConfirmedOrder(Convert.ToInt32(id), OrderStatus.Completed);
                        Console.WriteLine("Succesfully change order status");
                        Thread.Sleep(1300);
                        
                        return;
                    case "":
                        
                        return;
                    default:
                        Console.WriteLine("Incorrect button");
                        break;
                }
            }
        }
        public static void SetStatusConfirmedOrder(int id, OrderStatus orderStatus)
        {
            foreach (var ConfirmedOrder in Collections.ConfirmedOrders[id])
            {
                ConfirmedOrder.OrderStatus = orderStatus;
            }
        }
        public void WatchProducts()
        {
            MainFunctions.WatchProducts();
        }
        public static void AddNewProduct()
        {
            Console.WriteLine("Enter empty string for back");
            Console.WriteLine("Enter new product name:");
            string name = Console.ReadLine();
            if (name == "")
            {
               
                return;
            }
            if (name.Length < 3)
            {
                Console.WriteLine("Incorrect name , length must be more than 3");
                Console.WriteLine("Press empty string for back or 1 for repeat");
                while (true)
                {
                    switch (Console.ReadLine())
                    {
                        case "":
                            
                            return;
                        case "1":
                            AddNewProduct();
                            return;
                        default:
                            Console.WriteLine("Incorrect button");
                            break;
                    }
                }
            }
            Console.WriteLine("Enter new category from (Computers,Appliances,Smartphones) name:");
            string Category = Console.ReadLine();
            if (Category == "")
            {
                
                return;
            }
            List<string> Categories = new List<string>() { "Computers", "Appliances", "Smartphones" };
            if (!Categories.Contains(Category))
            {
                Console.WriteLine("Incorrect product category ");
                Console.WriteLine("Press empty string for back or 1 for repeat");
                while (true)
                {
                    switch (Console.ReadLine())
                    {
                        case "":
                            
                            return;
                        case "1":
                            AddNewProduct();
                            return;
                        default:
                            Console.WriteLine("Incorrect button");
                            break;
                    }
                }
            }
            Console.WriteLine("Enter new product description:");
            string description = Console.ReadLine();
            if(description=="")
            {
                
                return;
            }
            if (description.Length < 6)
            {
                Console.WriteLine("Incorrect description , length must be more than 5");
                Console.WriteLine("Press empty string for back or 1 for repeat");
                while (true)
                {
                    switch (Console.ReadLine())
                    {
                        case "":
                           
                            return;
                        case "1":
                            AddNewProduct();
                            return;
                        default:
                            Console.WriteLine("Incorrect button");
                            break;
                    }
                
            
            }   }
            Console.WriteLine("Enter new price for product");
            string price = Console.ReadLine();
            if (price == "")
            {
                MainFunctions.GoBack();
                return;
            }
            try
            {
                decimal Price = Convert.ToDecimal(price);

            }
            catch (System.OverflowException)
            {
                System.Console.WriteLine(
                    "The price value is too large .");
            }
            catch
            {
                Console.WriteLine("Incorrect price");
                Console.WriteLine("Press empty string for back or 1 for repeat");
                while (true)
                {
                    switch (Console.ReadLine())
                    {
                        case "":
                            MainFunctions.GoBack();
                            return;
                        case "1":
                            AddNewProduct();
                            return;
                        default:
                            Console.WriteLine("Incorrect button");
                            break;
                    }

                }
                }
            if (Category == "Computers") 
            {
                Collections.goods.Add(new Good { Id=Collections.goods.Count+1,Name = name, Category = Good.category.Computers, Description = description, 
                    Price = Convert.ToDecimal(price) });
              }
            if (Category == "Appliances")
            {
                Collections.goods.Add(new Good {
                    Id = Collections.goods.Count + 1,Name = name, 
                    Category = Good.category.Computers, Description = description, Price = Convert.ToDecimal(price)
                });
             }
            if (Category == "Smartphones")
            {
                Collections.goods.Add(new Good
                {
                    Id = Collections.goods.Count + 1,
                    Name = name,
                    Category = Good.category.Computers,
                    Description = description,
                    Price = Convert.ToDecimal(price)
                });
            }
            Console.WriteLine("Succesfully added new product ...");
            Thread.Sleep(1000);
            
        }
        public static void ChangeProductInfo()
        {
            Displayer.DisplayGoods();
            Console.WriteLine("Enter id of product to change or empty string for back");
            string id = Console.ReadLine();
            if (id == "")
            {
                MainFunctions.GoBack();
            }
            try
            {
                Convert.ToInt32(id);
            }
            catch
            {
                Console.WriteLine("Incorrect id of product ..");
                Thread.Sleep(1300);
                MainFunctions.GoBack();
            }
            if (!Collections.goods.Any(i => i.Id == Convert.ToInt32(id)))
            {
                Console.WriteLine("There is no product with such id");
                Console.WriteLine("Press 1 for repeat,empty string for back");
                while (true)
                {
                    switch (Console.ReadLine())
                    {
                        case "1":
                            ChangeProductInfo();
                            return;
                        case "":
                            MainFunctions.GoBack();
                            return;
                    }
                }
            }
            ChangeProductInfoById(id);

        }
        public static void ChangeProductInfoById(string id)
        {
            int Id = Convert.ToInt32(id);
            var good = Collections.goods.First(i => i.Id == Id);
            Console.WriteLine("1.Product Name:" + good.Name);
            Console.WriteLine("2.Product Category:" + good.Category);
            Console.WriteLine("3.Product Price:" + good.Price);
            Console.WriteLine("4.Product Description:" + good.Description);
            Console.WriteLine("   Enter field to change or empy string to back:");
            string number = Console.ReadLine();
            while (true)
            {
                switch (number)
                {
                    case "1":
                        Console.WriteLine("Enter new product name:");
                        string name = Console.ReadLine();
                        if (name == "")
                        {
                            MainFunctions.GoBack();
                            return;
                        }
                        if (name.Length < 3)
                        {
                            Console.WriteLine("Incorrect name , length must be more than 3");
                            Console.WriteLine("Press empty string for back or 1 for repeat");
                            while (true)
                            {
                                switch (Console.ReadLine())
                                {
                                    case "":
                                        MainFunctions.GoBack();
                                        return;
                                    case "1":
                                        ChangeProductInfoById(id);
                                        return;
                                    default:
                                        Console.WriteLine("Incorrect button");
                                        break;
                                }
                            }
                        }
                        Collections.goods.First(i => i.Id == Id).Name = name;
                        Console.WriteLine("Succesfully changed name");
                        
                        Thread.Sleep(1000);
                        MainFunctions.GoBack();
                        return;
                    case "2":
                        Console.WriteLine("Enter new category from (Computers,Appliances,Smartphones) name:");
                        string Category = Console.ReadLine();
                        if (Category == "")
                        {
                            MainFunctions.GoBack();
                            return;
                        }
                        List<string> Categories = new List<string>() { "Computers", "Appliances", "Smartphones" };
                        if (!Categories.Contains(Category))
                        {
                            Console.WriteLine("Incorrect product category ");
                            Console.WriteLine("Press empty string for back or 1 for repeat");
                            while (true)
                            {
                                switch (Console.ReadLine())
                                {
                                    case "":
                                        MainFunctions.GoBack();
                                        return;
                                    case "1":
                                        AddNewProduct();
                                        return;
                                    default:
                                        Console.WriteLine("Incorrect button");
                                        break;
                                }
                            }
                        }
                        if(Category=="Computers") Collections.goods.First(i => i.Id == Id)
                                .Category = Good.category.Computers;
                        if (Category == "Appliances") Collections.goods.First(i => i.Id == Id)
                                   .Category = Good.category.Appliances;
                        if (Category == "Smartphones") Collections.goods.First(i => i.Id == Id)
                                   .Category = Good.category.Smartphones;
                        Console.WriteLine("Succesful changed product category");
                        
                        Thread.Sleep(1000);
                        MainFunctions.GoBack();
                        return;
                        
                    case "3":
                        Console.WriteLine("Enter new price for product");
                        string price = Console.ReadLine();
                        if (price == "")
                        {
                            MainFunctions.GoBack();
                            return;
                        }
                        try
                        {
                            decimal Price = Convert.ToDecimal(price);

                        }
                        catch (System.OverflowException)
                        {
                            System.Console.WriteLine(
                                "The price value is too large .");
                        }
                        catch
                        {
                            Console.WriteLine("Incorrect price");
                            Console.WriteLine("Press empty string for back or 1 for repeat");
                            while (true)
                            {
                                switch (Console.ReadLine())
                                {
                                    case "":
                                        MainFunctions.GoBack();
                                        return;
                                    case "1":
                                        ChangeProductInfoById(id);
                                        return;
                                    default:
                                        Console.WriteLine("Incorrect button");
                                        break;
                                }

                            }
                        }
                        Collections.goods.First(i => i.Id == Id)
                                .Price = Convert.ToDecimal(price);
                        
                        Thread.Sleep(1000);
                        MainFunctions.GoBack();
                        return;
                    case "4":
                        Console.WriteLine("Enter new product description:");
                        string description = Console.ReadLine();
                        if (description == "")
                        {
                            MainFunctions.GoBack();
                            return;
                        }
                        if (description.Length < 6)
                        {
                            Console.WriteLine("Incorrect description , length must be more than 5");
                            Console.WriteLine("Press empty string for back or 1 for repeat");
                            while (true)
                            {
                                switch (Console.ReadLine())
                                {
                                    case "":
                                        MainFunctions.GoBack();
                                        return;
                                    case "1":
                                        ChangeProductInfoById(id);
                                        return;
                                    default:
                                        Console.WriteLine("Incorrect button");
                                        break;
                                }

                            }
                        }
                        Collections.goods.First(i => i.Id == Id)
                                .Description = description;
                        Console.WriteLine("Succesfully changed description for product");
                        
                        Thread.Sleep(1000);
                        MainFunctions.GoBack();
                        return;
                    case "":
                        MainFunctions.GoBack();
                        return;
                    default:
                        Console.WriteLine("Incorrect button");
                        break ;
                }
            }
        } 
    }
}
