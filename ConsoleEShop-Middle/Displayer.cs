﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
namespace ConsoleEShop_Middle
{
    class Displayer : IDisplay
    {
        public void DisplayOptions()
        {
            switch (CurrentUser.UserStatus) {
                case CurrentUser.Status.Guest:
                    Console.WriteLine("\n\n\n Shop Menu\n\n 1.View Products \n 2.Search products \n 3.Registration" +
                "\n 4.Log in\n 5.Exit");
                    break;
                case CurrentUser.Status.Logged:
                    Console.WriteLine("\n\n\n Shop Menu\n\n 1.View Products \n 2.Search products \n 3.Make new order " +
                "\n 4.Confirm order or cancel"+"\n 5.Watch history of confirmed orders and their status"+
                "\n 6.Set status for confirmed order"+"\n 7.Change personal info"+"\n 8.Quit"+
                "\n 9.Exit");
                    break;
                case CurrentUser.Status.Administator:
                    Console.WriteLine("\n\n\n Shop Menu\n\n 1.View Products \n 2.Search products \n 3.Make new order " +
                "\n 4.Confirm order or cancel" + "\n 5.Watch and change users personal info" +
                "\n 6.Add new good" + "\n 7.Change information about product"+"\n 8.Change status of order" + "\n 9.Quit" +
                "\n 10.Exit");
                    break;
            }
        }
        public static void DisplayGoods() 
        { 
            foreach(var good in Collections.goods)
            {
                DisplayGood(good);
            }
        }
        public static string DisplaySearchingGood() 
        {
            Console.WriteLine("Enter good name");
            string query = Console.ReadLine();
            Console.WriteLine("\n");
            if (query == "3")
            {
                return "3";
            }
            foreach (var good in Collections.goods)
            {
                if (good.Name.ToLower().Contains(query.ToLower()))
                {
                    DisplayGood(good);
                    return "Success";
                }

            }
            Console.WriteLine("Good was not found , Press 1 for repeat , 3 for back");
            while (true)
            {
                switch (Console.ReadLine())
                {
                    case "1":
                        string goodName = DisplaySearchingGood();
                        return goodName;
                    case "3":
                        return "3";
                    default:
                        Console.WriteLine("Incorrect button");
                        break;
                }
            }
        }
        private static void DisplayGood(Good good)
        {
            Console.WriteLine("Product Id: "+good.Id);
            Console.WriteLine("Name: " + good.Name);
            Console.WriteLine("Description: " + good.Description);
            Console.WriteLine("Category: " + good.Category);
            Console.WriteLine("Price: " + good.Price + " $\n\n");
        }
        public static void DisplayOrders(string username)
        {
            foreach(var order in Collections.orders.Where(i=>i.User.Username==username))
            {
                Console.WriteLine("Date:"+order.DateTime);
                DisplayGood(order.Good);
                Console.WriteLine("Status:"+order.OrderStatus);
                Console.WriteLine("\n");
            }
        }
        public static void DisplayConfirmedOrders(string username)
        {
            var query = Collections.ConfirmedOrders.Select(i => i.Value.Where(i => i.User.Username == username));
            var res = query.Select(i=>i).GroupBy(i=>new { Status = i.Select(i=>i.OrderStatus)}) ;
            foreach (var order in res )
            {
                var status = order.Key.Status.Select(i => i).First();
                var date = order.Select(i=>i).First().Select(i=>i.DateTime).First();
                var id = Collections.ConfirmedOrders.First(i => i.Value.Select(i => i.DateTime).First() == date).Key;
                Console.WriteLine("Id: "+id+"\n");
                Console.WriteLine("Status: " + status+"\n");
                Console.WriteLine("Date: "+date+"\n");
                foreach (var goods in order)
                {
                    var good = goods.Select(i => i);
                   foreach(var good1 in good)
                    {
                        DisplayGood(good1.Good);
                    }
                }
            }
        }
        public static void DisplayPersonalInfo(string username)
        {
            Console.WriteLine("1.Your name :"+ Collections.PersonalInfos[username].Name);
            Console.WriteLine("2.Your surname :" + Collections.PersonalInfos[username].Surname);
            Console.WriteLine("3.Your phone number :" + Collections.PersonalInfos[username].PhoneNumber);
            Console.WriteLine("4.Your adress :" + Collections.PersonalInfos[username].Adress);
            Console.WriteLine("5.Your email :" + Collections.PersonalInfos[username].Email);

        }
        public static void DisplayUsersPersonalInfo()
        {
            foreach(var order in Collections.PersonalInfos)
            {
                Console.WriteLine("Username: "+order.Key);
                Console.WriteLine("    Name: "+order.Value.Name);
                Console.WriteLine("    Surname: " + order.Value.Surname);
                Console.WriteLine("    Phone Number: " + order.Value.PhoneNumber);
                Console.WriteLine("    Adress: " + order.Value.Adress);
                Console.WriteLine("    Email: " + order.Value.Email);
            }
        }
    }
}
