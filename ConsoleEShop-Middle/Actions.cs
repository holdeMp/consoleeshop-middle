﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace ConsoleEShop_Middle
{
    public static class Actions
    {
        private static readonly Dictionary<CurrentUser.Status,Dictionary<string, Action>> UserActions =
    new Dictionary<CurrentUser.Status, Dictionary<string, Action>>() {
        { CurrentUser.Status.Guest, new Dictionary<string,Action>(){{"1", MainFunctions.WatchProducts },
                                                                    {"2", MainFunctions.SearchProduct },
                                                                    {"3", Guest.Registration },
                                                                    {"4", Guest.Login },
                                                                    {"5", Exit }
        } },
    { CurrentUser.Status.Logged, new Dictionary<string,Action>(){   {"1", MainFunctions.WatchProducts },
                                                                    {"2", MainFunctions.SearchProduct },
                                                                    {"3", RegisteredUserFunctions.MakeNewOrder},
                                                                    {"4", RegisteredUserFunctions.ConfirmOrCancelOrder },
                                                                    {"5", Logged.WatchHistoryAndStatus },
                                                                    {"6",Logged.SetReceivedStatus },
                                                                    {"7",Logged.ChangePersonalInfo },
                                                                    {"8",RegisteredUserFunctions.QuitFromAccount },
                                                                    {"9",Exit}
    }},
  { CurrentUser.Status.Administator, new Dictionary<string,Action>(){{"1", MainFunctions.WatchProducts },
                                                                    {"2", MainFunctions.SearchProduct },
                                                                    {"3", RegisteredUserFunctions.MakeNewOrder},
                                                                    {"4", RegisteredUserFunctions.ConfirmOrCancelOrder },
                                                                    {"5", Admin.ChangeUsersPersonalInfo },
                                                                    {"6", Admin.AddNewProduct },
                                                                    {"7", Admin.ChangeProductInfo },
                                                                    {"8", Admin.ChangeStatusOfOrder },
                                                                    {"9",RegisteredUserFunctions.QuitFromAccount},
                                                                    {"10",Exit }
    }
        } };
        public static void DoAction(string Option)
        {
            
            try
            {
                UserActions[CurrentUser.UserStatus][Option].Invoke();
            }
            catch
            {
                Console.WriteLine("Incorrect button");
                Thread.Sleep(900);
            }
        }
        private static void Exit()
        {
            Environment.Exit(1);
        }
    }
}
