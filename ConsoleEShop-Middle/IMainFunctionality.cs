﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle
{
    interface IMainFunctionality
    {
        void WatchProducts();
        void SearchProduct();
    }
}
