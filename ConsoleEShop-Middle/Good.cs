﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle
{
    public class Good
    {
        public int Id { get; set; }
        
        public enum category 
        { 
            Computers,
            Smartphones,
            Appliances
        }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public category Category { get; set; }

    }
}
