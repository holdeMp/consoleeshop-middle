﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle
{
    public static class CycleOptions
    {
        public static void Cycle()
        {
            try
            {
                Console.Clear();
            }
            catch 
            {
                Console.WriteLine("Incorrect desriptor");
            }
            Displayer displayer = new Displayer();
            displayer.DisplayOptions();
            DoAction();
        }
        static void DoAction()
        {
            Console.WriteLine("Enter option:");
            string Option = Console.ReadLine();
            Console.Clear();
            Actions.DoAction(Option);
            Cycle();
          
        }
    }
}
