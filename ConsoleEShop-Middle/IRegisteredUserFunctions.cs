﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle
{
    interface IRegisteredUserFunctions
    {
        void MakeNewOrder();
        void ConfirmOrCancelOrder();
        void QuitFromAccount();
    }
}
