﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;

namespace ConsoleEShop_Middle
{
    public static class RegisteredUserFunctions
    {
        public static void ConfirmOrCancelOrder()
        {
            if (!Collections.orders.Any())
            {
                Console.WriteLine("Your order is empy , make a new one.");
                Console.WriteLine("Press 3 for back");
                while (true)
                {
                    switch (Console.ReadLine())
                    {
                        case "3":
                            MainFunctions.GoBack();
                            return;
                        default:
                            Console.WriteLine("Incorrect button");
                            break;
                    }
                }
            }
            Console.WriteLine("Your order:");
            Displayer.DisplayOrders(CurrentUser.Username);
            Console.WriteLine("Press 1 for confirm , 2 for cancel ,3 for back");
            while (true)
            {
                switch (Console.ReadLine())
                {
                    case "1":
                        Collections.ConfirmedOrders.Add(Collections.ConfirmedOrders.Count + 1
                            , Collections.orders.FindAll(i => i.User.Username == CurrentUser.Username));
                        Collections.orders.RemoveAll(i => i.User.Username == CurrentUser.Username);
                        Console.WriteLine("Succesful confirming");
                        Thread.Sleep(1000);
                        return;
                    case "2":
                        foreach(var order in Collections.orders.Where(i => i.User.Username == CurrentUser.Username))
                        {
                            order.OrderStatus = OrderStatus.CanceledByUser;
                        }
                        Collections.ConfirmedOrders.Add(Collections.ConfirmedOrders.Count + 1
                            , Collections.orders.FindAll(i => i.User.Username == CurrentUser.Username));
                        Collections.orders.RemoveAll(i => i.User.Username == CurrentUser.Username);
                        Console.WriteLine("Succesful cancel");
                        Thread.Sleep(1000);
                        
                        return;
                    case "3":
                        
                        return;
                    default:
                        Console.WriteLine("Incorrect button");
                        break;
                }
            }
        }
        private static bool CheckGoodForExisting(string query)
        {
            foreach (var good in Collections.goods)
            {
                if (good.Name.ToLower().Contains(query.ToLower()))
                {

                    return true;
                }

            }
            return false;
        }

        public static void MakeNewOrder()
        {
            Displayer.DisplayGoods();
            Console.WriteLine("Press 3 for back");
            Console.WriteLine("\n Enter goodname:");

            string goodname = Console.ReadLine();
            bool check = CheckGoodForExisting(goodname);
            if (goodname == "3")
            {
                //MainFunctions.GoBack();
                return;
            }
            if (!check)
            {
                Console.WriteLine("Good was not found \n Press 1 for repeat , 3 for back");
                while (true)
                {
                    switch (Console.ReadLine())
                    {
                        case "1":
                            MakeNewOrder();
                            return;
                        case "3":
                            MainFunctions.GoBack();
                            return;
                        default:
                            Console.WriteLine("Incorrect button");
                            break;
                    }
                }
            }
            Collections.orders.Add(new Order
            {
                OrderStatus = OrderStatus.New,
                Good = Collections.goods.First(i => i.Name.ToLower().Contains(goodname.ToLower())),
                User = new User(CurrentUser.Username, null, CurrentUser.Email),
                DateTime = DateTime.Now
            });
            Console.WriteLine("Succesful order...");
            Thread.Sleep(1000);
            
            
        }
        public static void QuitFromAccount()
        {
            CurrentUser.Username = "";
            CurrentUser.UserStatus = CurrentUser.Status.Guest;
            CycleOptions.Cycle();
        }
    }
}
