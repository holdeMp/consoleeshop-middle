﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle
{
    public static class CurrentUser
    {
        public enum Status
        {
            Logged,
            Administator,
            Guest
        }
        public static Status UserStatus { get; set; } = Status.Guest;
        public static string Username { get; set; }
        public static string Password { get; set; }
        public static string Email { get; set; }
    }
}
