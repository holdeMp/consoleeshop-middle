﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle
{
    public enum OrderStatus
    {
        Received,
        New,
        CanceledByAdmin,
        Paid,
        Sent,
        Completed,
        CanceledByUser
    }
     public class Order
    {
        public OrderStatus OrderStatus { get; set; }
        public Good Good { get; set; }
        public User User { get; set; }
        public  DateTime DateTime { get; set; }
    }
}
