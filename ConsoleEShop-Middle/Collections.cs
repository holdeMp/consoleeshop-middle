﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle
{
    public static class Collections
    {
        public static Dictionary<string, User> RegisteredUsers { get; set; } = new Dictionary<string, User>() { {"admin",
        new User("admin","admin","admin@kjsd") },{ "zxcqwe",new User("zxcqwe","123456","zxcwqe@kjsd")} };
        public static List<Good> goods { get; set; } = new List<Good>()
        { new Good {
            Id = 1,
            Name = "Samsung Galaxy A51 6/128GB Blue" ,
            Description ="Screen (6.5 , Super AMOLED, 2400x1080) / Samsung Exynos 9611 (2.3 GHz + 1.7 GHz) " +
            "/ main quad camera: 48 MP + 12 MP + 5 MP + 5 MP, front 32 MP / RAM 6 GB / 128 GB built-in memory + microSD " +
            "(up to 512 GB) / 3G / LTE / GPS / support for 2 SIM-cards (Nano-SIM) / Android 10.0 (Q) / 4000 mAh",
            Category = Good.category.Smartphones,
            Price = 279},
        new Good {
            Id = 2,
            Name = "MSI PCI-Ex GeForce RTX 3080 Ti Suprim X 12G 12GB GDDR6X (384bit) (1830/19000)" ,
            Description ="Graphics chip GeForce RTX 3080 Ti Memory size 12 GB Memory bus width 384 bit Memory type GDDR6X Cooling system type Active",
            Category = Good.category.Computers,
            Price = 3487.63m},
        new Good {
            Id = 3,
            Name = "Built-in dishwasher BOSCH SMV25EX00E" ,
            Description ="Stable arrangement of cutlery, Espresso cups and small kitchenware."+
"The third level of loading. Replaces the cutlery basket. InfoLight® is an indicator that tells you when your dishwasher is running"
+"The illuminated dot on the floor under the dishwasher will remain on during the program and will automatically go out after the end of the program.",
            Category = Good.category.Appliances,
            Price = 477.22m}};
        public static List<Order> orders { get; set; } = new List<Order>();
        public static Dictionary<int,List<Order>> ConfirmedOrders { get; set; } = new Dictionary<int,List<Order>>();
        public static Dictionary<string, PersonalInfo> PersonalInfos { get; set; } = new Dictionary<string, PersonalInfo>()
        { { "admin",new PersonalInfo{ Email = "admin@kjsd"} } };

    }
}
